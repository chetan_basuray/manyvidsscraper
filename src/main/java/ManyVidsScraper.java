import com.google.common.collect.Maps;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.Map.Entry;
import java.util.LinkedHashMap;
import java.util.stream.Stream;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ManyVidsScraper {

    private String titles;

    public static void main(String[] args) {
        try{
            ManyVidsScraper manyVidsScraper = new ManyVidsScraper();
            WebDriver driver = new HtmlUnitDriver();

            ManyVidsScraper scrape = new ManyVidsScraper();
            String url = "https://www.manyvids.com/";

            //get the HTML document
            Document doc = Jsoup.connect(url).get();

            String string100Models = "";
            for(int i=1; i<=100; i++) {
                Thread.sleep(1000);
                string100Models += doc.select("#result-list > div:nth-child("+i+") > h4 > a").outerHtml()+";";
            }

            Map<String, String> data = Maps.newHashMap();
            data.put("mvtoken","5b1fdb677e484500221708");
            data.put("count","0");
            data.put("tag","All");
            data.put("isEditProfile","false");
            data.put("v","6");
            data.put("hasCrushMembership","1");

            String [] links = string100Models.split(";");
            System.out.println(links.length);
            for(String s: links){
                String link  = "https://www.manyvids.com" + s.substring(s.indexOf("href")+6, s.indexOf("title")-2).trim();
                String userId = s.split("/")[2];
                System.out.println(userId);
                System.out.println(link);
                data.put("uid",userId);
                Thread.sleep(2000);
                Document finalDoc = Jsoup.connect("https://www.manyvids.com/includes/append_small_vids.php")
                        .cookie("PHPSESSID","dlfpqs5qcn2n0u97fj3uer026e")
                        .cookie("AWSELB","7F55DF450E90ABB7C6B9CFC8E1B56B088115723F53CB20811E472F34A35338818BB3A876240068D7D8B72A7E5E057916C47BF68B12793E398477DAF2A8D585973900BF9B347A2A6D34C15DB0A52CCB42F92177E2AF")
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36")
                        .header("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                        .header("x-requested-with", "XMLHttpRequest")
                        .data(data)
                        .ignoreHttpErrors(true)
                        .ignoreContentType(true)
                        .post();

                manyVidsScraper.getTheTitles(finalDoc.text());
            }
            System.out.println(manyVidsScraper.titles);
            manyVidsScraper.countWords(manyVidsScraper.titles);
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

    private void getTheTitles(String document){
        try {
            JSONObject obj = new JSONObject(document);
            JSONArray arr = obj.getJSONArray("videoList");
            for (int i = 0; i < 15; i++) {
                titles += arr.getJSONObject(i).getString("title") + " ";
            }
            titles.replace(null, "");
        }catch(Exception e){

        }
    }

    private void countWords(String allTitles){
        Map<String, Long> wordCount = Arrays.stream(allTitles.split(" ")).collect(groupingBy(name -> name, counting()));

        for (Map.Entry<String, Long> entry : wordCount.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue().toString());
        }
    }

}
